== Dotfiles Repository

This assumes that Fedora Sway Atomic is being used as the operating system.

=== Install

Follow the instructions from the https://fedoraproject.org/atomic-desktops/sway/[Fedora Sway Atomic web site^].
Download the latest ISO and create a bootable USB stick.

Install from the bootable media; set location etc. to GB.

== Initial Configuration

After installation, and before configuring the wider system, there are some necessary first steps to take.

=== Hostname

First, set the host name for the machine.
This will be used in certificates etc.

----
sudo hostnamectl set-hostname <machine name>
----

=== GitLab

Configure SSH as the authentication mechanism for GitLab.

----
ssh-keygen -t ed25519
----

Once the key is generated, add the public key to the relevant repository (https://gitlab.com/[GitLab^]).

Test the connection and add the repository to the list of known hosts.

----
ssh -T git@gitlab.com
----

=== Shell & Utilities

Bat is a modern version of cat.
Cargo & Rust are the binaries for the rust language.
Neovim is the world's greatest editor and PDE.
RCM is a file management suite for dealing with dotfiles.
Essentially, dotfiles are stored in a git repository which then allows for easy recovery and set up.
Scdoc is the manual page compiler.
Z-Shell is my preferred shell.

Install them in the usual way with rpm-ostree:

----
rpm-ostree install bat cargo neovim rcm rust scdoc zsh
----

Change the default shell for the ‘user’ account to the Z-Shell.

----
sudo usermod -s /bin/zsh <username>
----

Now reboot to apply the changes.

Then clone the dotfiles from the repository and link them.

----
git clone git@gitlab.com:wyrdstapa/dotfiles.git ~/.local/share/rcm/dotfiles
rcup -d ~/.local/share/rcm/dotfiles -v config/rcm/rcrc zshenv; source ~/.zshenv; rcup -v
----

Re-load the shell to apply the new profile.

=== Shell Install

Having set up the basic capabilities for the operating system, we now need to configure the shell.

----
setup
----

This installs and configures the repositories.
Re-load the shell to apply the new profile.
Having done this, we can now install the applications.

----
inst
----

This will complete the shell set up.
This includes the installation of a number of plugins, flatpaks and local applications.

Reboot to cleanly apply the previous steps to the system.

== Final Configuration

Last steps to complete the set up of the system.

=== VPN

Configure Proton VPN using the flatpak.

NOTE: Due to a bug in Proton VPN, in order to login, temporarily set GNUPGHOME to ~/.gnupg

=== SDKMan

Set the auto environment settings to true for sdkman.

----
sdk config
----

=== LibreWolf

Enable the usual privacy settings.
Then customise it by adding the following extensions:

* uBlock Origin (should be installed by default)
* Privacy Badger
* Decentraleyes
* Cookie AutoDelete
* I Don't Care About Cookies
* ClearURLs
* Dark Reader
* Proton Pass
* Catppuccin Mocha - Blue theme (by catppuccin)

Restore the bookmarks from the backup.

