-------------------------------------------------------------------------------
-- Stephen Booth
-- init.lua - configuration file for yazi plugins.
-------------------------------------------------------------------------------

------------------- Modules

------------------- Plugin Configuration

-- Border
require("full-border"):setup()

-- Status lines
local catppuccin_theme = require("yatline-catppuccin"):setup("mocha")
require("yatline"):setup({
  -- Set the colour theme
  theme = catppuccin_theme,

  -- Display options
  show_background = true,

  display_header_line = true,
  display_status_line = true,

  -- Configure the header line
  header_line = {
    left = {
      section_a = {
        {type = "line", custom = false, name = "tabs", params = {"left"}},
      },
      section_b = {
      },
      section_c = {
        {type = "coloreds", custom = false, name = "githead"},
      }
    },
    right = {
      section_a = {
        {type = "string", custom = false, name = "date", params = {"%A, %d %B %Y"}},
      },
      section_b = {
        {type = "string", custom = false, name = "date", params = {"%X"}},
      },
      section_c = {
      }
    }
  },

  -- Configure the status line
  status_line = {
    left = {
      section_a = {
        {type = "string", custom = false, name = "tab_mode"},
      },
      section_b = {
        {type = "string", custom = false, name = "hovered_size"},
      },
      section_c = {
        {type = "string", custom = false, name = "hovered_name"},
        {type = "coloreds", custom = false, name = "count"},
      }
    },
    right = {
      section_a = {
        {type = "string", custom = false, name = "cursor_position"},
      },
      section_b = {
        {type = "string", custom = false, name = "cursor_percentage"},
      },
      section_c = {
        {type = "string", custom = false, name = "hovered_file_extension", params = {true}},
        {type = "coloreds", custom = false, name = "permissions"},
      }
    }
  },
})
require("yatline-githead"):setup()

