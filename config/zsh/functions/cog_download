#!/usr/bin/env zsh
###############################################################################
# Stephen Booth
# cog_download - download the specified file; extract if an archive; place the
#                output in the specified destination
#
# args:
#   --bin, -b   - the binary to extract
#   --dest, -d  - the destination directory for the output
#   --url, -u   - the URL to download from
###############################################################################

local _bin _dest _url

# Validate the arguments
while test $# -gt 0; do
    case $1 in
        --bin | -b)
            shift
            if [[ -z "$1" ]]; then
                cog_err "'--bin' option requires an argument"
                return 1
            fi
            _bin=$1
            ;;
        --dest | -d)
            shift
            if [[ -z "$1" ]]; then
                cog_err "'--dest' option requires an argument"
                return 1
            fi
            _dest=$1
            ;;
        --url | -u)
            shift
            if [[ -z "$1" ]]; then
                cog_err "'--url' option requires an argument"
                return 1
            fi
            _url=$1
            ;;
        *)
            ;;
    esac
    shift
done

[[ -z "$_dest" ]] && _dest="$(pwd)"
if [[ -z "$_url" ]]; then
    cog_err "url must be specified using '--url'"
    return 1
fi

# Download the file and process it according to type
cog_info "downloading: $_url"
case $_url in
    *.tar.gz)
        curl --silent --show-error --proto '=https' --fail --location "$_url" | tar xz -C "$_dest"
        ;;
    *.tar.xz | *.txz)
        curl --silent --show-error --proto '=https' --fail --location "$_url" | tar xJ -C "$_dest"
        ;;
    *.git)
        git clone --depth 1 -q "$_url" "$_dest"
        ;;
    *.gz)
        if [[ -z "$_bin" ]]; then
            cog_err "bin must be specified using '--bin'"
            return 1
        fi
        curl --silent --show-error --proto '=https' --fail --location "$_url" | gunzip -c > "$_dest/$_bin" && \
            chmod 755 "$_dest/$_bin"
        ;;
    *.jar)
        if [[ -z "$_bin" ]]; then
            cog_err "bin must be specified using '--bin'"
            return 1
        fi
        curl --silent --show-error --proto '=https' --fail --location --output "$_dest/$_bin" "$_url"
        ;;
    *.repo)
        sudo curl --silent --show-error --proto '=https' --fail --location \
            --output "/etc/yum.repos.d/${_url##*/}" "$_url"
        ;;
    *.rpm)
        rpm-ostree install "$_url"
        ;;
    *.zip)
        curl --silent --show-error --proto '=https' --fail --location --output "$_dest/temp.zip" "$_url" && \
            unzip -qq "$_dest/temp.zip" -d "$_dest" && \
            rm -f "$_dest/temp.zip"
        ;;
    *)
        if [[ -z "$_bin" ]]; then
            cog_err "bin must be specified using '--bin'"
            return 1
        fi
        curl --silent --show-error --proto '=https' --fail --location --output "$_dest/$_bin" "$_url" && \
            chmod 755 "$_dest/$_bin"
        ;;
esac

