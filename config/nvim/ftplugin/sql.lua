-------------------------------------------------------------------------------
-- Stephen Booth
-- Neovim configuration for SQL files.
-------------------------------------------------------------------------------

------------------- Modules

-- Completion
local cmp = require("cmp")

------------------- Configuration

-- Set up SQL command completion
local sources = cmp.config.sources({ { name = "sql" } })
cmp.setup.buffer { sources = sources }

