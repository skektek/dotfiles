-------------------------------------------------------------------------------
-- Stephen Booth
-- utils.lua - utility functions.
-------------------------------------------------------------------------------

------------------- Modules

-- Neovim
local b = vim.b
local cmd = vim.cmd

-- Plugins
local minimap = require("mini.map")

-- This module
local M = {}

------------------- Functions

-- Convert a table to a set
function M.toSet(list)
  local set = {}

  -- Iterate over the list and set the value as the element identifier
  for _, l in pairs(list) do
    set[l] = true
  end

  return set
end

-- Open a terminal in a tab
function M.terminal()
  -- Open a tab
  cmd.tabnew()
  -- Load a terminal and begin in insert mode
  cmd.terminal()
  cmd.startinsert()
  -- Turn off line numbers in the terminal
  cmd.setlocal("nonumber norelativenumber")
  -- Disable the minimap plugin
  b.minimap_disable = true
  minimap.close()
end

------------------- Return

return M

