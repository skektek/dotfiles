-------------------------------------------------------------------------------
-- Stephen Booth
-- Event driven commands.
-------------------------------------------------------------------------------

------------------- Modules

-- Neovim
local api = vim.api
local b = vim.b
local bo = vim.bo
local cmd = vim.cmd
local wo = vim.wo
-- Mini.nvim
local minimap = require("mini.map")
-- Local
local utils = require("utils")

------------------- Variables

-- Set of file types that should display the minimap etc.
local type_list = {
  "asciidoc", "conf", "css", "dosini", "dockerfile", "editorconfig", "gitignore", "html", "java", "jproperties",
  "kotlin", "lua", "sql", "systemd", "toml", "text", "yaml", "zsh"
}
local type_set = utils.toSet(type_list)

------------------- Autocommands

-- Git editor specific actions
api.nvim_create_augroup("gitcheck", { clear = true })
api.nvim_create_autocmd("FileType", {
  group = "gitcheck",
  pattern = { "gitcommit", "gitrebase" },
  command = "startinsert | 1"
})

-- Help actions
api.nvim_create_augroup("helpwindow", { clear = true })
api.nvim_create_autocmd("BufWinEnter", {
  group = "helpwindow",
  pattern = "*",
  callback = function(event)
    local filetype = bo[event.buf].filetype
    local file_path = event.match

    if file_path:match "/doc/" ~= nil then
      -- Only run if the file type is a help file
      if filetype == "help" or filetype == "markdown" then
        -- Open the help page in a new tab
        cmd.wincmd("T")
      end
    end
  end
})

-- Linter actions
api.nvim_create_augroup("linters", { clear = true })
api.nvim_create_autocmd({ "BufEnter", "BufWritePost", "InsertLeave" }, {
  group = "linters",
  pattern = "*",
  callback = function()
    require("lint").try_lint()
  end
})

-- Screen decoration specific actions
api.nvim_create_augroup("screencheck", { clear = true })
-- File type overrides
api.nvim_create_autocmd("BufEnter", {
  group = "screencheck",
  pattern = "*",
  callback = function()
    -- Test the file type and act accordingly
    if type_set[bo.filetype] then
      -- Turn on line length highlighting
      wo.colorcolumn = "+1"
      -- Open the minimap
      minimap.open()
    else
      -- Turn off line length highlighting
      if bo.filetype == "NeogitCommitMessage" then
        wo.colorcolumn = "+1"
      else
        wo.colorcolumn = ""
      end
      -- Disable the indent scope plugin
      b.miniindentscope_disable = true
      -- Disable the minimap plugin
      b.minimap_disable = true
      minimap.close()
    end
  end
})
api.nvim_create_autocmd("FileType", {
  group = "screencheck",
  pattern = { "NeogitCommitMessage" },
  callback = function()
    -- Turn on line length highlighting
    wo.colorcolumn = "+1"
  end
})

