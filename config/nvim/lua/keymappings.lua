-------------------------------------------------------------------------------
-- Stephen Booth
-- Configuration settings for key mappings.
-------------------------------------------------------------------------------

------------------- Modules

-- Which-Key
local wk = require("which-key")

------------------- Configuration

---------------------------------
-- Control Key mappings
---------------------------------
wk.add({
  -- BufferLine key mapping
  { "<c-n>", "<cmd>bnext<cr>", desc = "Next Buffer" },
  { "<c-p>", "<cmd>bprevious<cr>", desc = "Previous Buffer" },
  { "<c-d>", "<cmd>bdelete<cr>", desc = "Close Buffer" }
})

---------------------------------
-- Meta/Alt Key mappings
---------------------------------
wk.add({
  -- Window commands
  { "<m-h>", "<C-w>h", desc = "Go to the left window" },
  { "<m-j>", "<C-w>j", desc = "Go to the down window" },
  { "<m-k>", "<C-w>k", desc = "Go to the up window" },
  { "<m-l>", "<C-w>l", desc = "Go to the right window" },
  -- Tab key mapping
  { "<m-n>", "<cmd>tabnext<cr>", desc = "Next Tab" },
  { "<m-p>", "<cmd>tabprevious<cr>", desc = "Previous Tab" },
  { "<m-d>", "<cmd>tabclose<cr>", desc = "Close Tab" }
})

---------------------------------
-- Leader Key mappings
---------------------------------
-- Normal mode mappings
wk.add({
  { "<leader>", group = "leader-key" },
  -- Buffers
  { "<leader>1", "<cmd>LualineBuffersJump! 1<cr>", desc = "Jump to Buffer 1" },
  { "<leader>2", "<cmd>LualineBuffersJump! 2<cr>", desc = "Jump to Buffer 2" },
  { "<leader>3", "<cmd>LualineBuffersJump! 3<cr>", desc = "Jump to Buffer 3" },
  { "<leader>4", "<cmd>LualineBuffersJump! 4<cr>", desc = "Jump to Buffer 4" },
  { "<leader>5", "<cmd>LualineBuffersJump! 5<cr>", desc = "Jump to Buffer 5" },
  { "<leader>6", "<cmd>LualineBuffersJump! 6<cr>", desc = "Jump to Buffer 6" },
  { "<leader>7", "<cmd>LualineBuffersJump! 7<cr>", desc = "Jump to Buffer 7" },
  { "<leader>8", "<cmd>LualineBuffersJump! 8<cr>", desc = "Jump to Buffer 8" },
  { "<leader>9", "<cmd>LualineBuffersJump! 9<cr>", desc = "Jump to Buffer 9" },
  { "<leader>b", "<cmd>lua require('telescope.builtin').buffers()<cr>", desc = "List Buffers" },
  -- Code actions
  { "<leader>c", group = "code" },
  { "<leader>ca", "<cmd>lua vim.lsp.buf.code_action()<cr>", desc = "Code Action" },
  { "<leader>cn", ":IncRename ", desc = "Rename" },
  -- Files
  { "<leader>f", "<cmd>lua require('yazi').yazi(nil, vim.fn.getcwd())<cr>", desc = "File Manager" },
  -- Git
  { "<leader>g", group = "git" },
  { "<leader>gg", "<cmd>lua require('neogit').open()<cr>", desc = "Git" },
  { "<leader>gt", "<cmd>Gitsigns toggle_current_line_blame<cr>", desc = "Toggle Git Blame" },
  -- Help
  { "<leader>h", "<cmd>lua require('telescope.builtin').help_tags()<cr>", desc = "Help" },
  -- Find in files
  { "<leader>l", "<cmd>lua require('telescope.builtin').live_grep()<cr>", desc = "Live Grep" },
  -- Minimap
  { "<leader>m", group = "minimap" },
  { "<leader>mc", "<cmd>lua require('mini.map').close()<cr>", desc = "Close Minimap" },
  { "<leader>mf", "<cmd>lua require('mini.map').toggle_focus()<cr>", desc = "Toggle Focus to/from Minimap" },
  { "<leader>mo", "<cmd>lua require('mini.map').open()<cr>", desc = "Open Minimap" },
  { "<leader>mr", "<cmd>lua require('mini.map').refresh()<cr>", desc = "Refresh Minimap" },
  { "<leader>ms", "<cmd>lua require('mini.map').toggle_side()<cr>", desc = "Toggle Minimap Side (left/right)" },
  { "<leader>mt", "<cmd>lua require('mini.map').toggle()<cr>", desc = "Toggle Minimap" },
  -- Messages
  { "<leader>n", "<cmd>lua require('noice').cmd('telescope')<cr>", desc = "Show Messages" },
  -- Open files
  {
    "<leader>o",
    "<cmd>lua require('telescope.builtin').find_files({ find_command={'fd','--hidden','--type','file'} })<cr>",
    desc = "Open a File"
  },
  -- Projects
  {
    "<leader>p",
    "<cmd>lua require('telescope').extensions.project.project({ display_type = 'full' })<cr>",
    desc = "Open a Project"
  },
  -- Replace
  { "<leader>r", group = "replace" },
  {
    "<leader>rf",
    "<cmd>lua require('grug-far').grug_far({ prefills = { flags = vim.fn.expand('%') } })<cr>",
    desc = "Find and Replace (Current File)"
  },
  { "<leader>rr", "<cmd>lua require('grug-far').grug_far()<cr>", desc = "Find and Replace" },
  {
    "<leader>rw",
    "<cmd>lua require('grug-far').grug_far({ prefills = { search = vim.fn.expand('<cword>') } })<cr>",
    desc = "Find and Replace (Current Word)"
  },
  -- Terminal shell
  { "<leader>s", "<cmd>lua require('utils').terminal()<cr>", desc = "Open a Terminal Shell" },
  -- Tests
  { "<leader>t", group = "test" },
  { "<leader>ta", "<cmd>lua require('neotest').run.attach()<cr>", desc = "Attach to the Nearest Test" },
  { "<leader>tf", "<cmd>lua require('neotest').run.run(vim.fn.expand('%'))<cr>", desc = "Run the current Test File" },
  { "<leader>tk", "<cmd>lua require('neotest').run.stop()<cr>", desc = "Kill the Nearest Test" },
  { "<leader>tn", "<cmd>lua require('neotest').run.run()<cr>", desc = "Run the Nearest Test" },
  { "<leader>to", "<cmd>lua require('neotest').output.open({ enter = true })<cr>", desc = "Open the Output Window" },
  {
    "<leader>ts",
    "<cmd>lua require('neotest').summary.open()<cr><cmd>lua require('mini.map').close()<cr>",
    desc = "Toggle the Summary Window"
  },
  -- Undo
  { "<leader>u", "<cmd>lua require('telescope').extensions.undo.undo()<cr>", desc = "Undo History" },
  -- Options
  { "<leader>v", "<cmd>lua require('telescope.builtin').vim_options()<cr>", desc = "Vim Options" },
  -- Trouble
  { "<leader>x", group = "trouble" },
  {
    "<leader>xL",
    "<cmd>Trouble loclist open<cr><cmd>lua require('mini.map').close()<cr>",
    desc = "Location List (Trouble)"
  },
  {
    "<leader>xQ",
    "<cmd>Trouble quickfix open<cr><cmd>lua require('mini.map').close()<cr>",
    desc = "Quickfix List (Trouble)"
  },
  {
    "<leader>xb",
    "<cmd>Trouble diagnostics open filter.buf=0<cr><cmd>lua require('mini.map').close()<cr>",
    desc = "Buffer Diagnostics (Trouble)"
  },
  {
    "<leader>xl",
    "<cmd>Trouble lsp open<cr><cmd>lua require('mini.map').close()<cr>",
    desc = "LSP Definitions (Trouble)"
  },
  {
    "<leader>xs",
    "<cmd>Trouble symbols open<cr><cmd>lua require('mini.map').close()<cr>",
    desc = "Symbols (Trouble)"
  },
  {
    "<leader>xt",
    "<cmd>Trouble telescope open<cr><cmd>lua require('mini.map').close()<cr>",
    desc = "Telescope Results (Trouble)"
  },
  {
    "<leader>xw",
    "<cmd>Trouble diagnostics open<cr><cmd>lua require('mini.map').close()<cr>",
    desc = "Workspace Diagnostics (Trouble)"
  },
  -- Treesitter
  { "<leader>y", "<cmd>lua require('telescope.builtin').treesitter()<cr>", desc = "List Functions & Variables" }
})
-- Visual mode mappings
wk.add({
  {
    mode = { "v" },
    { "<leader>", group = "leader-key" },
    -- Code
    { "c", group = "code" },
    { "ca", "<cmd>lua vim.lsp.buf.range_code_action()<cr>", desc = "Code Action" },
    -- Replace
    { "<leader>r", group = "replace" },
    {
      "<leader>rf",
      "<cmd>lua require('grug-far').with_visual_selection({ prefills = { flags = vim.fn.expand('%') } })<cr>",
      desc = "Find and Replace (Current File)"
    },
    { "<leader>rr", "<cmd>lua require('grug-far').with_visual_selection()<cr>", desc = "Find and Replace" },
  }
})

