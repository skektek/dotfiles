-------------------------------------------------------------------------------
-- Stephen Booth
-- lsp.lua - plugins for language servers and related activities.
-------------------------------------------------------------------------------

------------------- Modules

------------------- Configuration

return {
  -- Tree sitter
  {
    "nvim-treesitter/nvim-treesitter", build = ":TSUpdate",
    dependencies = { "nvim-treesitter/nvim-treesitter-textobjects" },
    config = function()
      require("nvim-treesitter.configs").setup({
        auto_install = true,
        ensure_installed = {
          "bash",
          "c",
          "comment",
          "css",
          "diff",
          "dockerfile",
          "git_config",
          "git_rebase",
          "gitattributes",
          "gitcommit",
          "gitignore",
          "html",
          "ini",
          "java",
          "json",
          "kotlin",
          "lua",
          "markdown",
          "markdown_inline",
          "properties",
          "regex",
          "rust",
          "sql",
          "toml",
          "vim",
          "vimdoc",
          "xml",
          "yaml"
        },
        highlight = { enable = true },
        ignore_install = {},
        sync_install = false,
        textobjects = {
          lsp_interop = {
            enable = true,
            border = "rounded",
            floating_preview_opts = {},
            peek_definition_code = {
              ["<leader>df"] = "@function.outer",
              ["<leader>dF"] = "@class.outer",
            }
          }
        }
      })
    end
  },
  -- LSP Rename
  {
    "smjonas/inc-rename.nvim",
    config = function()
      require("inc_rename").setup()
    end,
  },
  -- Snippets
  {
    "L3MON4D3/LuaSnip", version = "*",
	  build = "make install_jsregexp",
    dependencies = {
      "rafamadriz/friendly-snippets",
      config = function()
        require("luasnip.loaders.from_vscode").lazy_load()
      end,
    },
    opts = {
      history = true,
      delete_check_events = "TextChanged",
    },
    keys = {
      {
        "<tab>",
        function()
          return require("luasnip").jumpable(1) and "<Plug>luasnip-jump-next" or "<tab>"
        end,
        expr = true, silent = true, mode = "i",
      },
      { "<tab>", function() require("luasnip").jump(1) end, mode = "s" },
      { "<s-tab>", function() require("luasnip").jump(-1) end, mode = { "i", "s" } }
    }
  },
  -- Code actions
  {
    "kosayoda/nvim-lightbulb", version = "*",
    config = function()
      require("nvim-lightbulb").setup({
        sign = { enabled = false },
        virtual_text = { enabled = true },
        number = { enabled = true },
        autocmd = { enabled = true }
      })
    end
  },
  -- Completion
  {
    "hrsh7th/nvim-cmp",
    dependencies = {
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-cmdline",
      "hrsh7th/cmp-emoji",
      "saadparwaiz1/cmp_luasnip",
      "chrisgrieser/cmp-nerdfont",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-nvim-lua",
      "hrsh7th/cmp-path",
      "ray-x/cmp-sql",
      "onsails/lspkind-nvim",
      "L3MON4D3/LuaSnip"
    },
    config = function()
      local cmp = require("cmp")
      local types = require("cmp.types")
      local lspkind = require("lspkind")
      local luasnip = require("luasnip")

      -- Configure completion
      cmp.setup({
        formatting = {
          format = lspkind.cmp_format({
            mode = "symbol_text",
            menu = ({
              emoji = "[Emoji]",
              nvim_lsp = "[LSP]",
              luasnip = "[LuaSnip]",
              nerdfont = "[NerdFont]",
              nvim_lua = "[Vim API]",
              path = "[Path]",
              sql = "[SQL]"
            })
          })
        },
        mapping = cmp.mapping.preset.insert({
          ["<Down>"] = cmp.mapping.select_next_item({ behavior = types.cmp.SelectBehavior.Select }),
          ["<Up>"] = cmp.mapping.select_prev_item({ behavior = types.cmp.SelectBehavior.Select }),
          ["<C-d>"] = cmp.mapping.scroll_docs(-4),
          ["<C-f>"] = cmp.mapping.scroll_docs(4),
          ["<C-Space>"] = cmp.mapping.complete(),
          ["<C-e>"] = cmp.mapping.abort(),
          ["<CR>"] = cmp.mapping.confirm({ select = true }),
        }),
        snippet = {
          expand = function(args)
            luasnip.lsp_expand(args.body)
          end
        },
        sources = cmp.config.sources({
          { name = "nvim_lsp" },
          { name = "nvim_lua" },
          { name = "luasnip" }
        }, {
          { name = "emoji", options = { insert = true } },
          { name = "nerdfont" },
          { name = "path" }
        })
      })

      -- Configure file search
      cmp.setup.cmdline({ "/", "?" }, {
        mapping = cmp.mapping.preset.cmdline(),
        sources = { { name = "buffer" } }
      })

      -- Configure neovim command line completion
      cmp.setup.cmdline(":", {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources({ { name = 'path' } }, { { name = 'cmdline' } }),
        matching = { disallow_symbol_nonprefix_matching = false }
      })
    end
  },
  -- Linters
  {
    "mfussenegger/nvim-lint",
    config = function()
      local lint = require("lint")

      -- Java linter configuration
      lint.linters.checkstyle.config_file = ".linter/checkstyle/checkstyle.xml"
      -- Kotlin linter configuration
      lint.linters.ktlint.args = { "--reporter=json", "--log-level=none", "--stdin" }
      -- Lua linter configuration
      lint.linters.selene.args = { "--config", ".linter/selene/selene.toml" }

      -- Associate linters with file types
      lint.linters_by_ft = {
        asciidoc = { "vale" },
        dockerfile = { "hadolint" },
        gitcommit = { "gitlint", "vale" },
        html = { "tidy" },
        java = { "checkstyle", "vale" },
        kotlin = { "ktlint" },
        lua = { "selene", "vale" },
        NeogitCommitMessage = { "gitlint", "vale" },
        sh = { "shellcheck" },
        sql = { "sqlfluff" },
        yaml = { "yamllint" },
        zsh = { "zsh" }
      }
    end
  },
  -- Tests
  { "rcasia/neotest-java", version = "*" },
  {
    "nvim-neotest/neotest", version = "*",
    dependencies = { "nvim-neotest/nvim-nio", "nvim-lua/plenary.nvim", "nvim-treesitter/nvim-treesitter" },
    config = function()
      require("neotest").setup({
        adapters = {
          require("neotest-java")({ ignore_wrapper = false })
        }
      })
    end
  },
  -- Mason and LSP configuration must be set up in a strict order, do not change these
  {
    "williamboman/mason.nvim", version = "*",
    config = function()
      require("mason").setup({
        ui = { border = "rounded" }
      })
    end
  },
  {
    "williamboman/mason-lspconfig.nvim", version = "*",
    dependencies = { "hrsh7th/cmp-nvim-lsp", "neovim/nvim-lspconfig", "williamboman/mason.nvim" },
    config = function()
      local lsp_capabilities = require("cmp_nvim_lsp").default_capabilities()
      local lspconfig = require("lspconfig")
      local mason_lspconfig = require("mason-lspconfig")

      -- Set up the server handlers
      mason_lspconfig.setup_handlers({
        -- Default handler
        function (server_name)
          lspconfig[server_name].setup({
            capabilities = lsp_capabilities,
            root_dir = lspconfig.util.root_pattern(".git", vim.fn.getcwd())
          })
        end,

        -- Specific set up for lua
        ["lua_ls"] = function ()
          lspconfig.lua_ls.setup({
            capabilities = lsp_capabilities,
            root_dir = lspconfig.util.root_pattern(".git", vim.fn.getcwd()),
            settings = {
              Lua = {
                runtime = { version = "LuaJIT" },
                diagnostics = { globals = { "vim" } },
                workspace = {
                  library = vim.api.nvim_get_runtime_file("", true),
                  checkThirdParty = false
                },
                telemetry = { enable = false }
              }
            }
          })
        end
      })
    end
  },
  {
    "WhoIsSethDaniel/mason-tool-installer.nvim",
    config = function()
      require("mason-tool-installer").setup({
        ensure_installed = {
          "bash-language-server",
          "checkstyle",
          "css-lsp",
          "dockerfile-language-server",
          "editorconfig-checker",
          "gitlint",
          "hadolint",
          "html-lsp",
          "jdtls",
          "ktlint",
          "lua-language-server",
          "rust-analyzer",
          "selene",
          "shellcheck",
          "sqlfluff",
          "vale",
          "vale-ls",
          "yaml-language-server",
          "yamllint"
        }
      })
    end
  },
  {
    "neovim/nvim-lspconfig", version = "*",
    config = function()
      local diagnostic = vim.diagnostic
      local fn = vim.fn

      -- Set the diagnostic signs
      fn.sign_define("DiagnosticSignError", { text = " ", texthl = "DiagnosticSignError", numhl = "" })
      fn.sign_define("DiagnosticSignHint", { text = "⚑ ", texthl = "DiagnosticSignHint", numhl = "" })
      fn.sign_define("DiagnosticSignInfo", { text = " ", texthl = "DiagnosticSignInfo", numhl = "" })
      fn.sign_define("DiagnosticSignWarn", { text = " ", texthl = "DiagnosticSignWarn", numhl = "" })

      -- Diagnostic configuration
      diagnostic.config({
        signs = true,
        underline = true,
        update_in_insert = false,
        virtual_text = { spacing = 4, prefix = "●" },
        severity_sort = true
      })
    end
  }
}

