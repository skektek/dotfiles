-------------------------------------------------------------------------------
-- Stephen Booth
-- style.lua - plugins for changing the editor style.
-------------------------------------------------------------------------------

------------------- Modules

------------------- Configuration

return {
  -- Theme
  {
    "catppuccin/nvim", name = "catppuccin", lazy = false, version = "*", priority = 1000,
    config = function()
      -- Catppuccin settings
      require("catppuccin").setup({
        flavour = "mocha",
        term_colors = false,
        integrations = {
          cmp = true,
          dashboard = true,
          gitsigns = true,
          indent_blankline = {
            enabled = true,
            colored_indent_levels = false
          },
          mason = true,
          mini = {
            enabled = true
          },
          neogit = true,
          neotest = true,
          noice = true,
          native_lsp = {
            enabled = true,
            virtual_text = {
              errors = { "italic" },
              hints = { "italic" },
              warnings = { "italic" },
              information = { "italic" },
              ok = { "italic" },
            },
            underlines = {
              errors = { "underline" },
              hints = { "underline" },
              warnings = { "underline" },
              information = { "underline" },
              ok = { "underline" },
            },
            inlay_hints = {
              background = true,
            },
          },
          notify = true,
          semantic_tokens = true,
          treesitter = true,
          telescope = { enabled = true },
          lsp_trouble = true,
          which_key = true
        },
      })
      -- Now set the colour scheme
      vim.cmd.colorscheme("catppuccin")
    end
  },
  -- Dashboard
  {
    "nvimdev/dashboard-nvim", version = "*", event = "VimEnter",
    opts = {
      theme = "hyper",
      config = {
        header = {
          "",
          "",
          " ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗",
          " ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║",
          " ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║",
          " ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║",
          " ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║",
          " ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝",
          "",
          ""
        },
        shortcut = {
          {
            desc = "  Projects", group = "DashboardShortcut", key = "1",
            action = "lua require('telescope').extensions.project.project({ display_type = 'full' })"
          },
          { desc = "  Packages", group = "DashboardShortcut", key = "2", action = "Lazy" },
          { desc = "  LSP", group = "DashboardShortcut", key = "3", action = "Mason" },
          { desc = "  Health", group = "DashboardShortcut", key = "4", action = "checkhealth" },
          { desc = "  Keys", group = "DashboardShortcut", key = "5", action = "WhichKey" },
          {
            desc = "󰘥  Help", group = "DashboardShortcut", key = "6",
            action = "lua require('telescope.builtin').help_tags()"
          },
          { desc = "󰗼  Quit", group = "DashboardShortcut", key = "7", action = "qa" },
        },
        packages = { enable = true },
        project = { limit = 4, action = "Telescope find_files cwd=" },
        mru = { limit = 4 },
        footer = function()
          return {
            "",
            "",
            "What you leave behind is not what is engraved in stone monuments,",
            "but what is woven into the lives of others. -- Pericles"
          }
        end
      },
      hide = { statusline = true, tabline = false, winbar = false }
    }
  },
  -- Blank lines
  {
    "lukas-reineke/indent-blankline.nvim", main = "ibl", version = "*",
    config = function()
      require("ibl").setup({
        exclude = { filetypes = { "dashboard"}},
        indent = { char = "┊" }
      })
    end
  },
  -- Status line
  {
    "nvim-lualine/lualine.nvim",
    config = function()
      local lualine = require("lualine")

      -- Customise the defaults
      lualine.setup({
        -- Global options
        options = { theme = "catppuccin" },
        -- Configure the status line sections
        sections = {
          lualine_a = { "mode" },
          lualine_b = { { "filename", path = 1 } },
          lualine_c = { "branch", { "diff", symbols = { added = " ", modified = " ", removed = " " } } },
          lualine_x = { "encoding", "fileformat", "filetype" },
          lualine_y = { "progress" },
          lualine_z = { "location" }
        },
        -- Enable the tab line, showing open buffers
        tabline = {
          lualine_a = { { "buffers", mode = 2 } },
          lualine_b = { },
          lualine_c = { },
          lualine_x = { { "diagnostics", sources = { "nvim_diagnostic" } } },
          lualine_y = { },
          lualine_z = { "tabs" }
        },
        -- Enable selected extensions
        extensions = { "lazy", "mason", "quickfix", "trouble" }
      })
    end
  }
}

