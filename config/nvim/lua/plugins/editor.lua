-------------------------------------------------------------------------------
-- Stephen Booth
-- editor.lua - plugins for enhancing the editor.
-------------------------------------------------------------------------------

------------------- Modules

------------------- Configuration

return {
  -- Plugin management
  { "folke/lazy.nvim", version = "*" },
  -- Plenary functions; used by pretty much everything, so reference first of all
  { "nvim-lua/plenary.nvim" },
  -- UI styling
  { "stevearc/dressing.nvim", version = "*", opts = {} },
  -- Git information
  {
    "lewis6991/gitsigns.nvim", version = "*",
    dependencies = { "nvim-lua/plenary.nvim" },
    config = function()
      local gitsigns = require("gitsigns")

      gitsigns.setup({
        signs = {
          add = { text = "▎" },
          change = { text = "▎" },
          delete = { text = "" },
          topdelete = { text = "" },
          changedelete = { text = "▎" },
          untracked = { text = "▎" }
        }
      })
    end
  },
  -- Find and replace
  {
    "MagicDuck/grug-far.nvim",
    config = function()
      require("grug-far").setup({
        windowCreationCommand = "tabnew %"
      });
    end
  },
  -- Small editor improvements
  {
    "echasnovski/mini.nvim", version = false,
    dependencies = { "lewis6991/gitsigns.nvim" },
    config = function()
      local icons = require("mini.icons")
      local indentscope = require("mini.indentscope")
      local map = require("mini.map")
      local pairs = require("mini.pairs")

      -- Enable icons
      icons.setup()
      MiniIcons.mock_nvim_web_devicons()

      -- Enable indent highlighting
      indentscope.setup()

      -- Enable the mini map
      map.setup({
        integrations = {
          map.gen_integration.builtin_search(),
          map.gen_integration.gitsigns(),
          map.gen_integration.diagnostic()
        },
        symbols = {
          encode = map.gen_encode_symbols.dot("3x2"),
          scroll_line = "▶",
          scroll_view = "┋"
        },
        window = { show_integration_count = false }
      })

      -- Enable pairs
      pairs.setup()
    end
  },
  -- Git integration
  { "sindrets/diffview.nvim" },
  {
    "NeogitOrg/neogit",
    dependencies = { "nvim-lua/plenary.nvim", "sindrets/diffview.nvim", "nvim-telescope/telescope.nvim" },
    config = true
  },
  -- Notifications
  {
    "folke/noice.nvim", version = "*",
    dependencies = { "MunifTanjim/nui.nvim", "rcarriga/nvim-notify" },
    config = function()
      local noice = require("noice")

      noice.setup({
        lsp = {
          -- Override markdown rendering so that **cmp** and other plugins use **Treesitter**
          override = {
            ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
            ["vim.lsp.util.stylize_markdown"] = true,
            ["cmp.entry.get_documentation"] = true
          },
        },
        presets = {
          bottom_search = false,
          command_palette = true,
          long_message_to_split = true,
          inc_rename = true,
          lsp_doc_border = true
        }
      })
    end
  },
  -- Neovim UI component library
  { "MunifTanjim/nui.nvim", version = "*" },
  -- Neovim notification manager
  {
    "rcarriga/nvim-notify", version = "*",
    opts = {
      timeout = 3000,
      max_height = function()
        return math.floor(vim.o.lines * 0.75)
      end,
      max_width = function()
        return math.floor(vim.o.columns * 0.75)
      end
    }
  },
  -- Fuzzy finder over lists
  { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
  { "nvim-telescope/telescope-project.nvim" },
  { "debugloop/telescope-undo.nvim" },
  {
    "nvim-telescope/telescope.nvim", version = "*",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope-fzf-native.nvim",
      "nvim-telescope/telescope-project.nvim",
      "debugloop/telescope-undo.nvim"
    },
    config = function()
      local telescope = require("telescope")
      local open_with_trouble = require("trouble.sources.telescope").open

      -- Configure telescope
      telescope.setup({
        -- Customise the defaults
        defaults = {
          mappings = {
            i = { ["<c-t>"] = open_with_trouble },
            n = { ["<c-t>"] = open_with_trouble },
          },
          path_display = { "smart" },
          prompt_prefix = "🔍 "
        },
        -- Configure pickers
        pickers = {
          find_files = {
            find_command = { "fd", "--exclude", ".git", "--hidden", "--type", "f" }
          },
          live_grep = {
            find_command = { "rg", "--vimgrep" }
          }
        },
        -- Configure extensions
        extensions = {
          project = {
            base_dirs = {
              { path = "$XDG_DATA_HOME/rcm/dotfiles" },
              { path = "~/Workspace/git" }
            },
            hidden_files = true
          },
          undo = {
            layout_strategy = "horizontal",
            layout_config = { preview_width = 0.7 }
          }
        }
      })

      -- Load extensions: the order matters here: we must load extensions after calling the setup function
      telescope.load_extension("fzf")
      telescope.load_extension("project")
      telescope.load_extension("undo")
    end
  },
  -- Diagnostic windows
  {
    "folke/trouble.nvim", version = "*",
    cmd = "Trouble",
    opts = {
      modes = {
        diagnostics = {
          focus = false,
          preview = {
            type = "split",
            relative = "win",
            position = "right",
            size = 0.4
          }
        },
        loclist = {
          focus = true,
          preview = {
            type = "split",
            relative = "win",
            position = "right",
            size = 0.4
          }
        },
        lsp = {
          focus = false,
          preview = {
            type = "split",
            relative = "win",
            position = "right",
            size = 0.4
          }
        },
        quickfix = {
          focus = true,
          preview = {
            type = "split",
            relative = "win",
            position = "right",
            size = 0.4
          }
        },
        symbols = {
          focus = false,
          preview = {
            type = "split",
            relative = "win",
            position = "bottom",
            size = 0.4
          }
        },
        telescope = {
          focus = true,
          preview = {
            type = "split",
            relative = "win",
            position = "right",
            size = 0.4
          }
        }
      }
    }
  },
  -- Key mappings
  {
    "folke/which-key.nvim", version = "*",
    opts = {
      preset = "classic",
      plugins = {
        spelling = { enabled = true, suggestions = 36 },
      },
      win = { border = "single" }
    }
  },
  -- File manager
  {
    "mikavilpas/yazi.nvim", version = "*",
    opts = {
      open_for_directories = true
    },
    config = function()
      require("yazi").setup()
    end
  }
}

