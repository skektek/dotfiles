-------------------------------------------------------------------------------
-- Stephen Booth
-- File detection for docker files.
-------------------------------------------------------------------------------

------------------- Modules

-- Neovim
local api = vim.api

------------------- Configuration

-- Docker file type detection
api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  pattern = { "Dockerfile", "*.Dockerfile" },
  command = "setlocal ft=dockerfile"
})

