-------------------------------------------------------------------------------
-- Stephen Booth
-- File detection for gradle files.
-------------------------------------------------------------------------------

------------------- Modules

-- Neovim
local api = vim.api

------------------- Configuration

-- Gradle file type detection
api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  pattern = { "*.gradle" },
  command = "setlocal ft=groovy"
})

