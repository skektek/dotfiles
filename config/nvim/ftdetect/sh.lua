-------------------------------------------------------------------------------
-- Stephen Booth
-- File detection for bash files.
-------------------------------------------------------------------------------

------------------- Modules

-- Neovim
local api = vim.api

------------------- Configuration

-- Bash file type detection
api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  pattern = { ".bashrc", ".profile", "*.bash", "*.sh" },
  command = "setlocal ft=sh"
})

