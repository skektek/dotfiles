#!/usr/bin/env zsh
###############################################################################
# Stephen Booth
# .zshenv - executed once only just prior to login. Place session-wide
# environment variables here.
###############################################################################

################### Environment Settings

# Set the umask: owner - full; group - read and execute; other - read and execute
umask 022

# XDG Base Directory Specification
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_DIRS="${XDG_CONFIG_DIRS:-/etc/xdg}"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_DIRS="${XDG_DATA_DIRS:-/usr/local/share/:/usr/share/}"
export XDG_DATA_HOME="/var/home/$USER/.local/share"
export XDG_RUNTIME_DIR="${XDG_RUNTIME_DIR:-/run/user/$UID}"
export XDG_STATE_HOME="$HOME/.local/state"

# XDG Operating System Specification
if [[ -e /etc/os-release ]]; then
    source /etc/os-release
else
    [[ -e /usr/lib/os-release ]] && source /usr/lib/os-release
fi

# ZShell local directories
typeset -Agx COG
COG[FLATPAK]="/var/lib/flatpak"
COG[LOCAL_DIR]="$HOME/.local"
COG[SYSTEMD]="$XDG_CONFIG_HOME/systemd/user"
COG[WORKSPACE]="$HOME/Workspace"

COG[APP_DIR]="$XDG_DATA_HOME/applications"
COG[BIN_DIR]="$COG[LOCAL_DIR]/bin"
COG[FLATPAK_BIN]="$COG[FLATPAK]/exports/bin"
COG[GIT]="$COG[WORKSPACE]/git"
COG[LIB_DIR]="$COG[LOCAL_DIR]/lib"
COG[MAN_DIR]="$XDG_DATA_HOME/man"
COG[POSTGRES]="$COG[WORKSPACE]/postgres"
COG[SYSTEMD_DEFAULT]="$COG[SYSTEMD]/default.target.wants"
COG[SYSTEMD_TIMERS]="$COG[SYSTEMD]/timers.target.wants"
COG[ZSH_CACHE]="$XDG_CACHE_HOME/zsh"
COG[ZSH_CONFIG]="$XDG_CONFIG_HOME/zsh"
COG[ZSH_DATA]="$XDG_DATA_HOME/zsh"
COG[ZSH_FUNCTIONS]="$COG[ZSH_CONFIG]/functions"
COG[ZSH_STATE]="$XDG_STATE_HOME/zsh"
COG[ZCOMPDUMP]="$COG[ZSH_CACHE]/zcompdump"

[[ -d $COG[APP_DIR] ]] || mkdir -p $COG[APP_DIR]
[[ -d $COG[BIN_DIR] ]] || mkdir -p $COG[BIN_DIR]
[[ -d $COG[GIT] ]] || mkdir -p $COG[GIT]
[[ -d $COG[POSTGRES] ]] || mkdir -p $COG[POSTGRES]
[[ -d $COG[SYSTEMD_DEFAULT] ]] || mkdir -p $COG[SYSTEMD_DEFAULT]
[[ -d $COG[SYSTEMD_TIMERS] ]] || mkdir -p $COG[SYSTEMD_TIMERS]
[[ -d $COG[ZSH_CACHE] ]] || mkdir -p $COG[ZSH_CACHE]
[[ -d $COG[ZSH_DATA] ]] || mkdir -p $COG[ZSH_DATA]
[[ -d $COG[ZSH_STATE] ]] || mkdir -p $COG[ZSH_STATE]

################### Locale

export LANG="en_GB.UTF-8"
export LANGUAGE="en_GB.UTF-8"
export LC_ALL="en_GB.UTF-8"

################### Browser

export BROWSER="flatpak run io.gitlab.librewolf-community"

################### Sheldon Plugin Manager

export SHELDON_CONFIG_DIR="$XDG_CONFIG_HOME/sheldon"
export SHELDON_DATA_DIR="$XDG_DATA_HOME/sheldon"

################### Utilities

# FZF
# Use fd for the default search (.gitignore is automatically respected).
export FZF_DEFAULT_COMMAND="fd --follow --type file"
export FZF_DEFAULT_OPTS="--border --header 'Find files' --prompt '🔍 ' \
--color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 \
--color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
--color=marker:#f5e0dc,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8"

export FZF_ALT_C_COMMAND="fd --follow --type directory"
export FZF_ALT_C_OPTS="--header 'Change directory' --preview 'tree -C {}'"

export FZF_CTRL_T_COMMAND="fd --follow --type file"
export FZF_CTRL_T_OPTS="--header 'Find files' --preview 'bat --color=always --style=numbers --line-range=:500 {}'"

# GPG
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
[[ -d $GNUPGHOME ]] || ( mkdir -p $GNUPGHOME && chmod 700 $GNUPGHOME )

# Less
export LESSHISTFILE="$COG[ZSH_STATE]/lesshst"

# RCM
export RCRC="$XDG_CONFIG_HOME/rcm/rcrc"

# Ripgrep
export RIPGREP_CONFIG_PATH="$XDG_CONFIG_HOME/ripgreprc"

# NSS PKI
export SSL_DIR="$XDG_DATA_HOME/pki"

################### Development Tools

# SDKMAN
export SDKMAN_DIR="$XDG_DATA_HOME/sdkman"

# Gradle
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"

# Java
export JAVA_HOME="$SDKMAN_DIR/candidates/java/current"

# Neovim
export NVIM_APPNAME="${NVIM_APPNAME:-nvim}"
export NVIM_HOME="${NVIM_HOME:-$XDG_DATA_HOME/$NVIM_APPNAME}"

# Npm
export NPM_CONFIG_CACHE="$XDG_CACHE_HOME/npm"
export NPM_CONFIG_TMP="$XDG_RUNTIME_DIR/npm"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"

# Nvm
export NVM_DIR="$XDG_CONFIG_HOME/nvm"

# PostgreSQL
export PSQL_HISTORY="$XDG_STATE_HOME/psql/history"
export PSQLRC="$XDG_CONFIG_HOME/psql/psqlrc"

# Rust
export CARGO_HOME="$XDG_DATA_HOME/cargo"

################### Path

typeset -U PATH path FPATH fpath
path=($COG[BIN_DIR] $COG[FLATPAK_BIN] $CARGO_HOME/bin $NVIM_HOME/mason/bin $path)
fpath=($COG[ZSH_FUNCTIONS].zwc $fpath)

################### Editor & Pager

# Editor: set to Neovim with a fallback to Vi
export EDITOR="${$(command -v nvim):-$(command -v vi)}"
[[ "$(basename $EDITOR)" = "nvim" ]] && export MERGE_EDITOR="$EDITOR -d"
export VISUAL="$EDITOR"

# Pager: set to less
export PAGER="$(command -v less)"
export MANPAGER="$PAGER"

