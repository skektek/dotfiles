-------------------------------------------------------------------------------
-- Stephen Booth
-- wezterm.lua - configuration file for the wezterm terminal emulator.
-------------------------------------------------------------------------------

------------------- Modules

-- Wezterm itself
local wezterm = require("wezterm")

------------------- Configuration

local config = wezterm.config_builder()

config.color_scheme = "Catppuccin Mocha"
config.default_cursor_style = "SteadyUnderline"
config.font = wezterm.font("IosevkaTerm Nerd Font")
config.font_size = 14.0
config.hide_tab_bar_if_only_one_tab = true

return config

